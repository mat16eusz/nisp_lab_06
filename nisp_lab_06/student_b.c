/**
 * @author    Adrian Ciochon
 * @author    34275@stundet.pwsztar.edu.pl
 */

#include "student_b.h"
#include <stdio.h>
#include <stdbool.h>

 /**
 * We check if the given number is odd.
 *
  * @param int digit: the number we want to check is odd.
  * @return bool: true if the number is odd, false if the number is even.
  */
bool is_odd(int digit)
{
    if (digit % 2 != 0)
    {
        return true;
    }

    return false;
}

/**
 * Search for min from an array of integers.
 *
 * @param int digits[]: an array of integers to find the lowest number from there.
 * @param int size: array size.
 * @return int: the lowest value in the array.
 */
int min(int digits[], unsigned size)
{
    for (int i = 0; i < size - 1; i++)
    {
        if (digits[0] > digits[i + 1])
        {
            int temp = digits[0];
            digits[0] = digits[i + 1];
            digits[i + 1] = temp;
        }
    }

    return digits[0];
}