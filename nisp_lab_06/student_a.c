/**
 * @author    Mateusz Jasiak
 * @author    mateusz.jasiak.dev@gmail.com
 */

#include "student_a.h"
#include <stdio.h>
#include <stdbool.h>

/**
 * We check if the given number is even.
 *
 * @param int digit: the number we want to check is even.
 * @return bool: true if the number is even, false if the number is not even.
 */
bool is_even(int digit)
{
	if (digit % 2 == 0)
	{
		return true;
	}

	return false;
}

/**
 * Search for max from an array of integers.
 *
 * @param int digits[]: an array of integers to find the largest number from there.
 * @param int size: array size.
 * @return int: the largest value in the array.
 */
int max(int digits[], unsigned size)
{
	for (int i = 0; i < size - 1; i++)
	{
		if (digits[0] < digits[i + 1])
		{
			int temp = digits[0];
			digits[0] = digits[i + 1];
			digits[i + 1] = temp;
		}
	}

	return digits[0];
}
