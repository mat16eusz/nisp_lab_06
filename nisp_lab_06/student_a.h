/**
 * @author    Mateusz Jasiak
 * @author    mateusz.jasiak.dev@gmail.com
 */

#ifndef STUDENT_A_H_
#define STUDENT_A_H_

#include <stdbool.h>

bool is_even(int digit);
int max(int digits[], unsigned size);

#endif
