/**
 * @author    Adrian Ciochon
 * @author    34275@student.pwsztar.edu.pl
 */

#ifndef STUDENT_B_H_
#define STUDENT_B_H_

#include <stdbool.h>

bool is_odd(int digit);
int min(int digits[], unsigned size);

#endif
