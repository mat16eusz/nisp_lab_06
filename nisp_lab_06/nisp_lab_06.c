﻿/**
 * @author    Mateusz Jasiak
 * @author    mateusz.jasiak.dev@gmail.com
 * @author    Adrian Ciochon
 * @author    34275@student.pwsztar.edu.pl
 */

#include <stdio.h>

#include "student_a.h"
#include "student_b.h"

#define SIZE_A 5
#define SIZE_B 9

int main()
{
    int number_a = 12;
	int number_b = 11;
    int digits_a[SIZE_A] = {1, 6, 2, 3, 4};
    int digits_b[SIZE_B] = { 8, 4, 9, 1, 3, -2, 5, 0, -1 };

    if (is_even(number_a))
    {
        printf("The number is even\n");
    }
    else
    {
        printf("The number is not even\n");
	}
	
	if (is_odd(number_b))
    {
        printf("The number is odd\n");
    }
    else
    {
        printf("The number is even\n");
    }

    printf("The largest value in the array is %d.\n", max(digits_a, SIZE_A));
    printf("The lowest value in the array is %d.\n", min(digits_b, SIZE_B));

    return 0;
}
